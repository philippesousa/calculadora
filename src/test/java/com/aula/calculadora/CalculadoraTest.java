package com.aula.calculadora;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class CalculadoraTest {


	@Test
	public void somarTest() {
		//fazer uma soma que resulta 10!
		Long a = 5L; 
		Long b = 5L;
		Long expected = Calculadora.somar(a, b);
		Assert.assertTrue(expected == 10L);
	}

	@Test
	public void subtrairTest() {
		//fazer uma subtracao que tenha diferena  5!
		Long a = 10L; 
		Long b = 5L;
		Long expected = Calculadora.subtrair(a,b);
		Assert.assertTrue(expected == 5L);	}

	@Test
	public void dividirTest() {
		//fazer uma divisao com produto 10!
		Long a = 100L; 
		Long b = 10L;
		Long expected = Calculadora.dividir(a, b);
		Assert.assertTrue(expected == 10L);
	}

	@Test
	public void multiplicarTest() {
		//fazer uma multiplicacao que tenho o quaciente 10!
		Long a = 7L; 
		Long b = 10L;
		Long expected = Calculadora.multiplicar(a, b);
		Assert.assertTrue(expected == 70L);	}

}
